package com.example.publicfeedapp.ui.addpost;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.asksira.bsimagepicker.Utils;
import com.bumptech.glide.Glide;
import com.example.publicfeedapp.R;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.example.publicfeedapp.model.SaveDataResult;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddPostActivity extends AppCompatActivity implements ImagesAdapter.ItemActionListener,
        BSImagePicker.OnSingleImageSelectedListener, BSImagePicker.ImageLoaderDelegate{
    private static final int REQUEST_LOCATION = 2;

    EditText postEditText;
    private AddPostViewModel postViewModel;
    private ImagesAdapter mAdapter;
    private RecyclerView recyclerView;
    private List<String> imagesLists = new ArrayList<>();
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        postViewModel = new ViewModelProvider(this)
                .get(AddPostViewModel.class);
        final LinearLayout btnLocation = findViewById(R.id.btnLocation);
        final LinearLayout btnPhotos = findViewById(R.id.btnPhotos);
        recyclerView = findViewById(R.id.images_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        postViewModel.getPostData().observe(this, new Observer<Post>() {
            @Override
            public void onChanged(@Nullable Post postData) {
                if (postData == null) {
                    return;
                }
                btnLocation.setSelected(postData.getType()==PostType.LOCATION);
                btnPhotos.setSelected(postData.getType()==PostType.IMAGE);
                btnPhotos.setActivated(postData.getType()!=PostType.TEXT);
                btnLocation.setActivated(postData.getType()!=PostType.TEXT);
                findViewById(R.id.imagesPanel).setVisibility(postData.getType()==PostType.IMAGE?View.VISIBLE:View.GONE);
                switch (postData.getType()){
                    case PostType.IMAGE:
                        if(mAdapter==null){
                            mAdapter = new ImagesAdapter(AddPostActivity.this,imagesLists,AddPostActivity.this);
                            recyclerView.setAdapter(mAdapter);
                        }
                        imagesLists.clear(); //если вам необходимо очищать список при каждом запросе
                        imagesLists.addAll(postData.getImages());
                        if(imagesLists.size()<5){
                            imagesLists.add(null);
                        }
                        mAdapter.notifyDataSetChanged();
                        break;
                    case PostType.TEXT:
                        break;
                    case PostType.LOCATION:
                        break;

                }
            }
        });

        postViewModel.getSaveResult().observe(this, new Observer<SaveDataResult>() {
            @Override
            public void onChanged(@Nullable SaveDataResult saveResult) {
                if (saveResult == null) {
                    return;
                }
                if (saveResult.getError() != null) {
                    Toast.makeText(AddPostActivity.this, saveResult.getError(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddPostActivity.this, "Data saved", Toast.LENGTH_SHORT).show();
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });

        postEditText = findViewById(R.id.postText);
        postEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                postViewModel.changePostBody(postEditText.getText().toString());
            }
        });
        btnPhotos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            postViewModel.changePostType(PostType.IMAGE);
            }
        });
        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCurrentLocation();
                postViewModel.changePostType(PostType.LOCATION);
            }
        });
    }

    void getCurrentLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Display UI and wait for user interaction
            } else {
                ActivityCompat.requestPermissions(
                        this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_LOCATION);
            }
        } else {
            // permission has been granted, continue as usual
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(AddPostActivity.this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                postViewModel.addLocation(location);
                                Toast.makeText(AddPostActivity.this, "Location changed", Toast.LENGTH_SHORT).show();
                            }else{
                                requestLocation();
                            }
                        }
                    });
        }
    }

    private void requestLocation(){
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);
        LocationCallback locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        postViewModel.addLocation(location);
                        Toast.makeText(AddPostActivity.this, "Location changed", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if(grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                getCurrentLocation();
            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_post, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_save_post:
                postViewModel.save();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemAction(int action, int itemPosition) {
        switch (action){
            case ImagesAdapter.ItemActionListener
                    .ACTION_ADD_IMAGE:
                BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.example.publicfeedapp.fileprovider")
                        .setMaximumDisplayingImages(Integer.MAX_VALUE)
                        .setSpanCount(3)
                        .setGridSpacing(Utils.dp2px(2))
                        .setPeekHeight(Utils.dp2px(360))
                        .hideGalleryTile()
                        .setTag("A request ID")
                        .useFrontCamera()
                        .build();
                singleSelectionPicker.show(getSupportFragmentManager(), "picker");
                break;
            case ImagesAdapter.ItemActionListener
                    .ACTION_DELETE:
                postViewModel.removeImagePath(itemPosition);
                break;
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        postViewModel.addImagePath(uri);
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(AddPostActivity.this).load(imageUri).into(ivImage);
    }

}