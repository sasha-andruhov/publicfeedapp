package com.example.publicfeedapp.ui.userprofile;

import android.net.Uri;

import androidx.annotation.Nullable;

class ProfileFormState {
    @Nullable
    private Uri avatarPath;

    @Nullable
    private String userNickname;

    private boolean isDataChanged=false;
    private boolean isAvatarChanged=false;

    ProfileFormState(@Nullable Uri avatarPath, @Nullable String userNickname) {
        this.avatarPath = avatarPath;
        this.userNickname = userNickname;
    }


    @Nullable
    String getUserNickname() {
        return userNickname;
    }

    void setNickName(String nickname){
        this.userNickname = nickname;
        this.isDataChanged = true;
    }


    @Nullable
    Uri getAvatarPath() {
        return avatarPath;
    }

    void setAvatar(Uri path){
        this.avatarPath = path;
        this.isDataChanged = true;
        this.isAvatarChanged = true;
    }

    boolean isDataChanged() {
        return isDataChanged;
    }
    boolean isAvatarChanged() {
        return isAvatarChanged;
    }
}