package com.example.publicfeedapp.ui.userprofile;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.example.publicfeedapp.model.SaveDataResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.concurrent.Executor;

public class ProfileViewModel extends ViewModel   implements Executor{

    private MutableLiveData<ProfileFormState> profileFormState;
    private MutableLiveData<SaveDataResult> saveResult = new MutableLiveData<>();

    LiveData<ProfileFormState> getProfileFormState() {
        if(profileFormState==null){
            profileFormState = new MutableLiveData<>();
            loadUserData();
        }
        return profileFormState;
    }

    LiveData<SaveDataResult> getSaveResult() {
        return saveResult;
    }

    private void loadUserData() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser!=null){
            profileFormState.setValue(new ProfileFormState(currentUser.getPhotoUrl(),currentUser.getDisplayName()));
        }
    }


    private void saveUserData(ProfileFormState dataState){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(dataState.getUserNickname())
                .setPhotoUri(dataState.getAvatarPath())
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            saveResult.setValue(new SaveDataResult(true));
                        }else{
                            saveResult.setValue(new SaveDataResult(task.getException().toString()));
                        }
                    }
                });
    }

    public void save() {
        final ProfileFormState dataState = profileFormState.getValue();
        if(dataState!=null){
            if(dataState.isAvatarChanged()){
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageReference = storage.getReference();
                final StorageReference reference = storageReference.child("images/profiles/" + currentUser.getUid());
                reference.putFile(dataState.getAvatarPath()).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                dataState.setAvatar(uri);
                               saveUserData(dataState);
                            }


                        });
                    }
                });
            }else{
                saveUserData(dataState);
            }
        }
    }

    public void changeAvatarPath(Uri path){
        ProfileFormState newState = profileFormState.getValue();
        newState.setAvatar(path);
        profileFormState.setValue(newState);

    }

    public void userNicknameChanged(String nickname) {
        ProfileFormState newState = profileFormState.getValue();
        newState.setNickName(nickname);
        profileFormState.setValue(newState);
    }

    @Override
    public void execute(@NonNull Runnable r) {
        r.run();
    }
}