package com.example.publicfeedapp.ui.addpost;

import android.location.Location;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.example.publicfeedapp.model.SaveDataResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Executor;

public class AddPostViewModel extends ViewModel   implements Executor{
    private static final String LOG_TAG = "AddPostViewModel";

    private MutableLiveData<Post> postData;
    private MutableLiveData<SaveDataResult> saveResult = new MutableLiveData<>();
    private Post savedData;

    LiveData<Post> getPostData() {
        if(postData==null){
            postData = new MutableLiveData<>();
            Post post = new Post();
            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
            if(currentUser!=null){
                post.setAuthorUid(currentUser.getUid());
                post.setAuthorName(currentUser.getDisplayName());
                if(currentUser.getPhotoUrl()!=null)
                    post.setAuthorPhotoUri(currentUser.getPhotoUrl().toString());
            }
            postData.setValue(post);
        }
        return postData;
    }

    LiveData<SaveDataResult> getSaveResult() {
        return saveResult;
    }



    public void changePostType(int postType){
        Post post = postData.getValue();
        post.setType(postType);
        postData.setValue(post);
    }

    public void changePostBody(String body){
        Post post = postData.getValue();
        post.setBody(body);
        postData.setValue(post);
    }

    public void addImagePath(Uri path){
        Post post = postData.getValue();
        if(post!=null){
//            post.getImages().add(path.getPath());
            post.getImages().add(path.toString());
            postData.setValue(post);
        }
    }
    public void removeImagePath(int index){
        Post post = postData.getValue();
        if(post!=null){
            post.getImages().remove(index);
            postData.setValue(post);
        }
    }

    public void addLocation(Location location){
        Post post = postData.getValue();
        post.setLatitude(location.getLatitude());
        post.setLongitude(location.getLongitude());
        postData.setValue(post);
    }


    private void savePost(){
        DatabaseReference mDatabase =  FirebaseDatabase.getInstance().getReference();
        String key = mDatabase.child("posts").push().getKey();
        savedData.setUid(key);
        savedData.setDate((System.currentTimeMillis() / 1000));
        Map<String, Object> postValues = savedData.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/posts/" + key, postValues);
        mDatabase.updateChildren(childUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    saveResult.setValue(new SaveDataResult(true));
                }else{
                    saveResult.setValue(new SaveDataResult(task.getException().toString()));
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                saveResult.setValue(new SaveDataResult("Upload photo failed:"));
                Log.e(LOG_TAG,"Upload photo fFailed: "+e.getCause());
            }

        });
    }

    private void uploadImage(final List<String> mediaUris, final int index){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageReference = storage.getReference();
        final StorageReference reference = storageReference.child("images/posts/" + UUID.randomUUID().toString());
        reference.putFile(Uri.parse(mediaUris.get(index))).
                addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                savedData.getImages().set(index,uri.toString());
//                                savedData.getImages().set(index,uri.getPath());
                                if(index < mediaUris.size()-1) {
                                    uploadImage(mediaUris,index+1); //Recursion
                                }else{
                                   savePost();
                                }
                            }


                        });
                    }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                saveResult.setValue(new SaveDataResult("Upload photo failed:"));
                Log.e(LOG_TAG,"Upload photo fFailed: "+e.getCause());
            }

        });
    }

    public void save() {
        savedData = postData.getValue();
        if(savedData==null){
            saveResult.setValue(new SaveDataResult("Wrong data"));
        }else{
            if(savedData.getType()== PostType.IMAGE){
                uploadImage(savedData.getImages(),0);
            }else{
                savePost();
            }
        }

    }

    @Override
    public void execute(@NonNull Runnable r) {
        r.run();
    }
}