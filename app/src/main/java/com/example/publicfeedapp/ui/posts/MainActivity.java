package com.example.publicfeedapp.ui.posts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.publicfeedapp.R;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.ui.addpost.AddPostActivity;
import com.example.publicfeedapp.ui.login.LoginActivity;
import com.example.publicfeedapp.ui.userprofile.UserProfileActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PostAdapter.ItemActionListener {
    private static final int RC_SIGN_IN = 9001;

    private RecyclerView recyclerView;
    private FirebaseAuth mAuth;
    private List<Post> postsLists = new ArrayList<>();
    private PostAdapter mAdapter;
    private PostsViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PostAdapter(postsLists, this);
        recyclerView.setAdapter(mAdapter);
        mAuth = FirebaseAuth.getInstance();
        viewModel = new ViewModelProvider(this).get(PostsViewModel.class);
        viewModel.getPosts().observe(this, new Observer<List<Post>>() {
            @Override
            public void onChanged(@Nullable List<Post> postsList) {
                postsLists.clear(); //если вам необходимо очищать список при каждом запросе
                if(postsList!=null){
                    postsLists.addAll(postsList);
                    mAdapter.notifyDataSetChanged();
                }
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int id = layoutManager.findLastCompletelyVisibleItemPosition();
                if(id>=postsLists.size()-1){
                    viewModel.loadPageData(mAdapter.getLastItemDate());
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser==null){
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, RC_SIGN_IN);
        }else{
            mAdapter.setUserUuid(currentUser.getUid());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_post:
                Intent intent = new Intent(this, AddPostActivity.class);
                startActivityForResult(intent, RC_SIGN_IN);
                break;
            case R.id.menu_user_profile:
                Intent intentProfile = new Intent(this, UserProfileActivity.class);
                startActivity(intentProfile);
                break;
            case R.id.menu_sign_out:
                FirebaseAuth.getInstance().signOut();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN&&resultCode==RESULT_OK){
            viewModel.loadPostsFromFirebase();
        }
    }

    @Override
    public void onItemLike(int itemPosition) {
        viewModel.setPostLike(postsLists.get(itemPosition).getUid());
    }

    @Override
    public void onItemImageClick(int itemPosition, int imagePosition) {
        Intent imageInt = new Intent(this, ImageViewActivity.class);
        imageInt.putStringArrayListExtra("images",new ArrayList<String>(postsLists.get(itemPosition).getImages()));
        imageInt.putExtra("imagePosition", imagePosition);
        startActivity(imageInt);

    }

}