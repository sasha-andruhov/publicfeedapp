package com.example.publicfeedapp.ui.addpost;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.publicfeedapp.R;
import java.util.List;



public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ViewHolder>  {
    private Context context;
    private List<String> items;
    private ItemActionListener mListener;


    public ImagesAdapter(Context context, List<String> items, ItemActionListener actionListener)  {
        this.context = context;
        this.items = items;
        this.mListener = actionListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder  {
        private View view;
        private ImageView image, delete;

        public ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            image = view.findViewById(R.id.image);
            delete = view.findViewById(R.id.delete);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public void onBindViewHolder(ImagesAdapter.ViewHolder holder, final int position) {
        if (items.get(position)==null){
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.image_plus);
            Glide.with(context).load("").apply(requestOptions).into(holder.image);
            holder.delete.setVisibility(View.GONE);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemAction(ItemActionListener.ACTION_ADD_IMAGE,-1);
                }
            });
        } else {
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemAction(ItemActionListener.ACTION_DELETE,position);
                }
            });
            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.image_plus);
            Glide.with(context)
                    .load(Uri.parse(items.get(position)))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .fitCenter()
                    .apply(requestOptions)
                    .into(holder.image);
        }

    }

    @Override
    public ImagesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ImagesAdapter.ViewHolder viewHolder = new ImagesAdapter.ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_image, parent, false));
        return viewHolder;
    }

    public interface ItemActionListener {
        int ACTION_DELETE = 0;
        int ACTION_ADD_IMAGE = 1;
        void onItemAction(int action, int itemPosition);
    }
}