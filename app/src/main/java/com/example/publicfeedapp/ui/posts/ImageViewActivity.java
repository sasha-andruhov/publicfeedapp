package com.example.publicfeedapp.ui.posts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.publicfeedapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class ImageViewActivity extends AppCompatActivity {
    private static final String TAG = "ImageViewActivity";
    private SliderView sliderView;
    private ArrayList<String> imagesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Intent inParams = getIntent();
        imagesList = new ArrayList<>();
        int imagePosition=-1;
        if(inParams!=null){
            imagesList = inParams.getStringArrayListExtra("images");
            imagePosition = inParams.getIntExtra("imagePosition",-1);
        }

        sliderView = findViewById(R.id.imageSlider);
        SliderImageAdapter adapter = new SliderImageAdapter(null,1);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();
        adapter.renewItems(imagesList);
        if(imagePosition!=-1)
            sliderView.setCurrentPagePosition(imagePosition);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_image_view, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.menu_download_image:
                try{
                    downloadCurrentImage();
                }catch (IOException e){
                    Log.e(TAG,"download photo fFailed: "+e.getCause());
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void downloadCurrentImage() throws IOException {
        String EXTERNAL_PICTURE_RELATIVE_PATH = Environment.DIRECTORY_PICTURES + File.separator + getString(R.string.app_name);

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference islandRef = storage.getReferenceFromUrl(imagesList.get(sliderView.getCurrentPagePosition()));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
            String title = "image_"+ UUID.randomUUID();
            ContentResolver resolver = this.getApplicationContext().getContentResolver();
            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, title);
            contentValues.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            contentValues.put(MediaStore.Images.Media.RELATIVE_PATH, EXTERNAL_PICTURE_RELATIVE_PATH);
            Uri uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
            islandRef.getFile(uri).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(getApplicationContext(), "Image downloaded", Toast.LENGTH_SHORT).show();
                    // Local temp file has been created
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG,"download photo fFailed: "+exception.getCause());
                    // Handle any errors
                }
            });
        }else{
            File direct = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"image_"+ UUID.randomUUID());
            islandRef.getFile(direct).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(getApplicationContext(), "Image downloaded", Toast.LENGTH_SHORT).show();
                    // Local temp file has been created
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.e(TAG,"download photo fFailed: "+exception.getCause());
                    // Handle any errors
                }
            });
        }
    }
}