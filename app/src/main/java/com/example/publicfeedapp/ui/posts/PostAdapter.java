package com.example.publicfeedapp.ui.posts;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.publicfeedapp.R;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import static android.graphics.Typeface.BOLD;
import static android.graphics.Typeface.NORMAL;

public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final List<Post> list;
    private Context context;
    ItemActionListener mListener;
    String userUuid;


    public PostAdapter(List<Post> list, ItemActionListener actionListener) {
        this.list = list;
        this.mListener = actionListener;
        this.userUuid = userUuid;
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case PostType.LOCATION:
                View viewLocation = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_post_location, parent, false);
                return new PostLocationHolder(viewLocation);
            case PostType.IMAGE:
                View viewImage = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_post_image, parent, false);
                return new PostImageViewHolder(viewImage);
            default:
                View viewText = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_post_text, parent, false);
                return new PostTextViewHolder(viewText);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case PostType.TEXT:
                ((PostTextViewHolder)holder).bind(list.get(position), position);
                break;
            case PostType.LOCATION:
                ((PostLocationHolder)holder).bind(list.get(position),position);
                break;
            case PostType.IMAGE:
                ((PostImageViewHolder)holder).bind(list.get(position),position);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public long getLastItemDate() {
        return list.get(list.size()-1).getDate();
    }

    public void setUserUuid(String userUuid){
        this.userUuid = userUuid;
        notifyDataSetChanged();
    }

    static String getDateStr(long date){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        long then = date;
        long now = new Date().getTime()/1000;

//        long seconds = (now - then)/1000;
        long seconds = (now - then);
        long minutes = seconds/60;
        long hours = minutes/60;
        long days = hours/24;

        String friendly = null;
        long num = 0;
        if (days > 0) {
            if(isDateInCurrentWeek(date)){
                return new SimpleDateFormat("EEEE 'at' K:mm a", Locale.ENGLISH).format(new Date(date*1000)).replace("AM", "am").replace("PM","pm");
            }else{
                return new SimpleDateFormat("dd MMM 'at' K:mm a", Locale.ENGLISH).format(new Date(date*1000)).replace("AM", "am").replace("PM","pm");
            }
        } else if (hours > 0) {
            num = hours;
            friendly = hours + " hour";
        } else if (minutes > 0) {
            num = minutes;
            friendly = minutes + " minutes";
        } else {
            num = seconds;
            friendly = seconds + " second";
        }
        if (num > 1) {
            friendly += "s";
        }
        String postTimeStamp = friendly + " ago";
        return postTimeStamp;
    }

    private static boolean isDateInCurrentWeek(long dateTimestamp) {
        Date date = new Date(dateTimestamp*1000);
        Calendar currentCalendar = Calendar.getInstance();
        int week = currentCalendar.get(Calendar.WEEK_OF_YEAR);
        int year = currentCalendar.get(Calendar.YEAR);
        Calendar targetCalendar = Calendar.getInstance();
        targetCalendar.setTime(date);
        int targetWeek = targetCalendar.get(Calendar.WEEK_OF_YEAR);
        int targetYear = targetCalendar.get(Calendar.YEAR);
        return week == targetWeek && year == targetYear;
    }

    class PostTextViewHolder extends RecyclerView.ViewHolder {

        ImageView authorPhoto, imageLike;
        TextView textBody, textAuthorName, textDate, textLikedBy;

        public PostTextViewHolder(View itemView) {
            super(itemView);
            authorPhoto = itemView.findViewById(R.id.authorPhoto);
            imageLike = itemView.findViewById(R.id.imageLike);
            textBody = itemView.findViewById(R.id.textBody);
            textAuthorName = itemView.findViewById(R.id.textAuthorName);
            textDate = itemView.findViewById(R.id.textDate);
            textLikedBy = itemView.findViewById(R.id.textLikedBy);
        }

        public void bind(Post post, final int position) {
            Glide.with(itemView.getContext()).load(post.getAuthorPhotoUri()).into(authorPhoto);
            textAuthorName.setText(post.getAuthorName());
            textBody.setText(post.getBody());
            textDate.setText(getDateStr(post.getDate()));
            if(post.getStars().size()>0){
                String usersStr = "";
                Iterator<String> iterator = post.getStars().values().iterator();
                int k=0;
                while (iterator.hasNext()&&k!=2) {
                    k++;
                    usersStr = usersStr+(k==1?"":",")+iterator.next();
                }
                SpannableStringBuilder spannable = new SpannableStringBuilder("Linked by "+usersStr);
                int spanLength = spannable.length();
                if(post.getStars().size()>2){
                    spannable.insert(spanLength," and ");
                    spannable.insert(spanLength+5, Integer.toString(post.getStars().size()-2) +" others");
                    spannable.setSpan(new StyleSpan(NORMAL), spanLength, spanLength+5, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    spannable.setSpan(new StyleSpan(BOLD), spanLength+5, spannable.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                }
                spannable.setSpan(new StyleSpan(BOLD), 10, spanLength, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

                textLikedBy.setText(spannable);
                textLikedBy.setVisibility(View.VISIBLE);
            }else{
                textLikedBy.setVisibility(View.GONE);
            }
            if(userUuid!=null&&post.getStars().containsKey(userUuid)){
                imageLike.setImageResource(R.drawable.ic_like);
            }else{
                imageLike.setImageResource(R.drawable.ic_like_border);
            }
            imageLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemLike(position);
                }
            });
        }
    }

    class PostLocationHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {

        MapView mapView;
        GoogleMap map;
        View layout;
        ImageView authorPhoto, imageLike;
        TextView textBody, textAuthorName, textDate, textLikedBy;

        private PostLocationHolder(View itemView) {
            super(itemView);
            layout = itemView;
            mapView = layout.findViewById(R.id.liteRowMap);
            authorPhoto = itemView.findViewById(R.id.authorPhoto);
            imageLike = itemView.findViewById(R.id.imageLike);
            textBody = itemView.findViewById(R.id.textBody);
            textAuthorName = itemView.findViewById(R.id.textAuthorName);
            textDate = itemView.findViewById(R.id.textDate);
            textLikedBy = itemView.findViewById(R.id.textLikedBy);
            if (mapView != null) {
                mapView.onCreate(null);
                mapView.getMapAsync(this);

            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(context);
            map = googleMap;
            setMapLocation();
        }

        private void setMapLocation() {
            if (map == null) return;

            LatLng data = (LatLng) mapView.getTag();
            if (data == null) return;

            // Add a marker for this item and set the camera
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(data, 15f));
            map.addMarker(new MarkerOptions().position(data));

            // Set the map type back to normal.
            map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            map.getUiSettings().setMapToolbarEnabled(false);
        }

        private void bind(Post post, final int position) {
            // Store a reference of the ViewHolder object in the layout.
            layout.setTag(this);
            // Store a reference to the item in the mapView's tag. We use it to get the
            // coordinate of a location, when setting the map location.
            mapView.setTag(new LatLng(post.getLatitude(),post.getLongitude()));
            setMapLocation();
            Glide.with(itemView.getContext()).load(post.getAuthorPhotoUri()).into(authorPhoto);
            textAuthorName.setText(post.getAuthorName());
            textBody.setText(post.getBody());
            textDate.setText(getDateStr(post.getDate()));
            if(post.getStars().size()>0){
                String usersStr = "";
                Iterator<String> iterator = post.getStars().values().iterator();
                int k=0;
                while (iterator.hasNext()&&k!=2) {
                    k++;
                    usersStr = usersStr+(k==1?"":",")+iterator.next();
                }
                SpannableStringBuilder spannable = new SpannableStringBuilder("Linked by "+usersStr);
                int spanLength = spannable.length();
                if(post.getStars().size()>2){
                    spannable.insert(spanLength," and ");
                    spannable.insert(spanLength+5, Integer.toString(post.getStars().size()-2) +" others");
                    spannable.setSpan(new StyleSpan(NORMAL), spanLength, spanLength+5, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    spannable.setSpan(new StyleSpan(BOLD), spanLength+5, spannable.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                }
                spannable.setSpan(new StyleSpan(BOLD), 10, spanLength, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

                textLikedBy.setText(spannable);
                textLikedBy.setVisibility(View.VISIBLE);
            }else{
                textLikedBy.setVisibility(View.GONE);
            }
            if(userUuid!=null&&post.getStars().containsKey(userUuid)){
                imageLike.setImageResource(R.drawable.ic_like);
            }else{
                imageLike.setImageResource(R.drawable.ic_like_border);
            }
            imageLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemLike(position);
                }
            });
        }
    }

    class PostImageViewHolder extends RecyclerView.ViewHolder {

        ImageView authorPhoto, imageLike;
        TextView textBody, textAuthorName, textDate, textLikedBy;
        SliderView sliderView;
        View itemView;

        public PostImageViewHolder(View itemView) {
            super(itemView);
            authorPhoto = itemView.findViewById(R.id.authorPhoto);
            imageLike = itemView.findViewById(R.id.imageLike);
            textBody = itemView.findViewById(R.id.textBody);
            textAuthorName = itemView.findViewById(R.id.textAuthorName);
            textDate = itemView.findViewById(R.id.textDate);
            textLikedBy = itemView.findViewById(R.id.textLikedBy);
            sliderView = itemView.findViewById(R.id.imageSlider);
            this.itemView = itemView;
        }

        public void bind(Post post, final int position) {
            Glide.with(itemView.getContext()).load(post.getAuthorPhotoUri()).into(authorPhoto);
            textAuthorName.setText(post.getAuthorName());
            textBody.setText(post.getBody());
            textDate.setText(getDateStr(post.getDate()));
            if(post.getStars().size()>0){
                String usersStr = "";
                Iterator<String> iterator = post.getStars().values().iterator();
                int k=0;
                while (iterator.hasNext()&&k!=2) {
                    k++;
                    usersStr = usersStr+(k==1?"":",")+iterator.next();
                }
                SpannableStringBuilder spannable = new SpannableStringBuilder("Linked by "+usersStr);
                int spanLength = spannable.length();
                if(post.getStars().size()>2){
                    spannable.insert(spanLength," and ");
                    spannable.insert(spanLength+5, Integer.toString(post.getStars().size()-2) +" others");
                    spannable.setSpan(new StyleSpan(NORMAL), spanLength, spanLength+5, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    spannable.setSpan(new StyleSpan(BOLD), spanLength+5, spannable.length(), Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                }
                spannable.setSpan(new StyleSpan(BOLD), 10, spanLength, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

                textLikedBy.setText(spannable);
                textLikedBy.setVisibility(View.VISIBLE);
            }else{
                textLikedBy.setVisibility(View.GONE);
            }
            if(userUuid!=null&&post.getStars().containsKey(userUuid)){
                imageLike.setImageResource(R.drawable.ic_like);
            }else{
                imageLike.setImageResource(R.drawable.ic_like_border);
            }
            imageLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemLike(position);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListener!=null)mListener.onItemImageClick(position,sliderView.getCurrentPagePosition());
                }
            });




            SliderImageAdapter adapter = new SliderImageAdapter(context,0);

            sliderView.setSliderAdapter(adapter);

            sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM); //set indicator animation by using IndicatorAnimationType. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
            sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
            sliderView.setIndicatorSelectedColor(Color.WHITE);
            sliderView.setIndicatorUnselectedColor(Color.GRAY);
            adapter.renewItems(post.getImages());

        }

    }

    public interface ItemActionListener{
        void onItemLike(int itemPosition);
        void onItemImageClick(int itemPosition, int imagePosition);
    }
}

