package com.example.publicfeedapp.ui.userprofile;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.asksira.bsimagepicker.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.publicfeedapp.R;
import com.example.publicfeedapp.model.SaveDataResult;

import java.util.Objects;

public class UserProfileActivity extends AppCompatActivity implements BSImagePicker.OnSingleImageSelectedListener, BSImagePicker.ImageLoaderDelegate{
    private static final String TAG = "UserProfileActivity";


    private ProfileViewModel profileViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        profileViewModel = new ViewModelProvider(this)
                .get(ProfileViewModel.class);
        final Button saveButton = findViewById(R.id.save);
        final EditText userNickNameEditText = findViewById(R.id.user_nickname);
        final ImageView userAvatarImage = findViewById(R.id.user_avatar);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        profileViewModel.getProfileFormState().observe(this, new Observer<ProfileFormState>() {
            @Override
            public void onChanged(@Nullable ProfileFormState profileFormState) {
                if (profileFormState == null) {
                    return;
                }
                saveButton.setEnabled(profileFormState.isDataChanged());
                if (profileFormState.getUserNickname() != null&&
                        !profileFormState.getUserNickname().equals(userNickNameEditText.getText().toString())) {
                    userNickNameEditText.setText(profileFormState.getUserNickname());
                }
                RequestOptions requestOptions = new RequestOptions()
                        .placeholder(R.drawable.image_plus);
                Glide.with(UserProfileActivity.this)
                        .load(profileFormState.getAvatarPath())
                        .apply(requestOptions)
                        .into(userAvatarImage);
            }
        });
        userNickNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                profileViewModel.userNicknameChanged(userNickNameEditText.getText().toString());
            }
        });

        profileViewModel.getSaveResult().observe(this, new Observer<SaveDataResult>() {
            @Override
            public void onChanged(@Nullable SaveDataResult saveResult) {
                if (saveResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (saveResult.getError() != null) {
                    Toast.makeText(UserProfileActivity.this, saveResult.getError(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(UserProfileActivity.this, "Data saved", Toast.LENGTH_SHORT).show();
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });


        userAvatarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.example.publicfeedapp.fileprovider")
//                        .setMaximumDisplayingImages(24) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                        .setMaximumDisplayingImages(Integer.MAX_VALUE) //Default: Integer.MAX_VALUE. Don't worry about performance :)
                        .setSpanCount(3) //Default: 3. This is the number of columns
                        .setGridSpacing(Utils.dp2px(2)) //Default: 2dp. Remember to pass in a value in pixel.
                        .setPeekHeight(Utils.dp2px(360)) //Default: 360dp. This is the initial height of the dialog.
//                        .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                        .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                        .setTag("A request ID") //Default: null. Set this if you need to identify which picker is calling back your fragment / activity.
                        .useFrontCamera()
                        .build();
                singleSelectionPicker.show(getSupportFragmentManager(), "picker");
            }
        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                profileViewModel.save();
            }
        });
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        profileViewModel.changeAvatarPath(uri);
    }

    @Override
    public void loadImage(Uri imageUri, ImageView ivImage) {
        Glide.with(UserProfileActivity.this).load(imageUri).into(ivImage);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}