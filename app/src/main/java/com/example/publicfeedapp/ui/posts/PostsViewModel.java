package com.example.publicfeedapp.ui.posts;


import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.publicfeedapp.data.database.DbOpenHelper;
import com.example.publicfeedapp.data.database.entities.PostLike;
import com.example.publicfeedapp.data.database.resolvers.PostRelationsGetResolver;
import com.example.publicfeedapp.data.database.resolvers.PostRelationsPutResolver;
import com.example.publicfeedapp.data.database.resolvers.PreferenceRelationsGetResolver;
import com.example.publicfeedapp.data.database.resolvers.PreferenceRelationsPutResolver;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.data.database.tables.PreferenceTable;
import com.example.publicfeedapp.data.repository.FirebaseDatabaseRepository;
import com.example.publicfeedapp.data.repository.PostRepository;
import com.example.publicfeedapp.model.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.pushtorefresh.storio3.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.delete.DefaultDeleteResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.Query;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.BackpressureStrategy;
import io.reactivex.disposables.Disposable;

public class PostsViewModel extends AndroidViewModel {

    private MutableLiveData<List<Post>> posts;
    private MutableLiveData<List<Post>> updatedData =new MutableLiveData<>();
    private PostRepository repository = new PostRepository();
    public static final int postsPageCount = 5;
    private List<Post> postsList;
    private StorIOSQLite storIOSQLite;
    private long dateUpdate = 0;

    public PostsViewModel(Application application) {
        super(application);
        storIOSQLite = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(new DbOpenHelper(application))
                .addTypeMapping(Post.class, SQLiteTypeMapping.<Post>builder()
                        .putResolver(new PostRelationsPutResolver())
                        .getResolver(new PostRelationsGetResolver())
                        .deleteResolver(new DefaultDeleteResolver<Post>() {
                            @NonNull
                            @Override
                            protected DeleteQuery mapToDeleteQuery(@NonNull Post object) {
                                return null;
                            }
                        })
                        .build())
                .addTypeMapping(Long.class, SQLiteTypeMapping.<Long>builder()
                        .putResolver(new PreferenceRelationsPutResolver())
                        .getResolver(new PreferenceRelationsGetResolver())
                        .deleteResolver(new DefaultDeleteResolver<Long>() {
                            @NonNull
                            @Override
                            protected DeleteQuery mapToDeleteQuery(@NonNull Long object) {
                                return null;
                            }
                        })
                        .build())
                .build();
        Long dateUpdate =  storIOSQLite.get()
                .object(Long.class)
                .withQuery(
                        Query.builder()
                                .table(PreferenceTable.TABLE)
                                .limit(1)
                                .build()
                )
                .prepare()
                .executeAsBlocking();
        if(dateUpdate!=null){
            this.dateUpdate = dateUpdate;
        }
        loadPostsFromFirebase();

    }


    public LiveData<List<Post>> getPosts() {
        if (posts == null) {
            posts = new MutableLiveData<>();
            postsList = new ArrayList<>();
            loadPosts();
        }
        return posts;
    }

    @Override
    protected void onCleared() {
        repository.removeListener();
        storIOSQLite
                .put()
                .object(dateUpdate)
                .prepare()
                .executeAsBlocking();
    }

    public void loadPostsFromFirebase() {
        repository.getData(dateUpdate);
        repository.addListener(new FirebaseDatabaseRepository.FirebaseDatabaseRepositoryCallback<Post>() {
            @Override
            public void onSuccess(List<Post> result) {
                if(result.size()>0){
                    PutResults<Post> dbResult= storIOSQLite
                            .put()
                            .objects(result)
                            .prepare()
                            .executeAsBlocking();
                    for (Map.Entry<Post, PutResult> pair: dbResult.results().entrySet()) {
                        if(pair.getValue().wasInserted()){
                            postsList.add(0,pair.getKey());
                        }else{
                            int index = postsList.indexOf(pair.getKey());
                            if(index!=-1){
                                postsList.remove(index);
                                postsList.add(index,pair.getKey());
                            }
                        }
                        posts.setValue(postsList);
                    }
                    dateUpdate = (System.currentTimeMillis() / 1000);
                    repository.removeListener();
                    repository.changeListenerDateUpdate(dateUpdate);
                }
            }

            @Override
            public void onError(Exception e) {
            }
        });
    }

    private void loadPosts() {
        List<Post> result =  storIOSQLite.get()
                .listOfObjects(Post.class)
                .withQuery(
                        Query.builder()
                                .table(PostTable.TABLE)
                                .orderBy(PostTable.COLUMN_DATE+" DESC")
                                .limit(postsPageCount)
                                .build()
                )
                .prepare()
                .executeAsBlocking();
        postsList.clear();
        postsList.addAll(result);
        posts.setValue(result);
    }

    public void loadPageData(long id){
        List<Post> result =  storIOSQLite.get()
                .listOfObjects(Post.class)
                .withQuery(
                        Query.builder()
                                .table(PostTable.TABLE)
                                .where(PostTable.COLUMN_DATE+" < ?")
                                .whereArgs(id)
                                .orderBy(PostTable.COLUMN_DATE+" DESC")
                                .limit(postsPageCount)
                                .build()
                )
                .prepare()
                .executeAsBlocking();
//        postsList.clear();
        postsList.addAll(result);
        posts.setValue(postsList);
    }

    public void setPostLike(String postUid){
        DatabaseReference postRef =  FirebaseDatabase.getInstance().getReference().child("posts").child(postUid);
        final FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.getStars().containsKey(currentUser.getUid())) {
                    // Unstar the post and remove self from stars
                    p.setStarCount(p.getStarCount() - 1);
                    p.getStars().remove(currentUser.getUid());
                } else {
                    // Star the post and add self to stars
                    p.setStarCount(p.getStarCount() + 1);
                    p.getStars().put(currentUser.getUid(), currentUser.getDisplayName());
                }
                p.setUpdatedAt((System.currentTimeMillis() / 1000));
                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed,
                                   DataSnapshot currentData) {
                // Transaction completed
                Log.d("PostsViewModel", "postTransaction:onComplete:" + databaseError);
            }
        });
    }
}
