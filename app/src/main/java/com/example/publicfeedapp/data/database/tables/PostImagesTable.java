package com.example.publicfeedapp.data.database.tables;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

public class PostImagesTable {

    @NonNull
    public static final String TABLE = "posts_images";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_POST_ID = "post_id";

    @NonNull
    public static final String COLUMN_IMAGE_URI = "image_uri";


    // This is just class with Meta Data, we don't need instances
    private PostImagesTable() {
        throw new IllegalStateException("No instances please");
    }

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_POST_ID + " TEXT NOT NULL, "
                + COLUMN_IMAGE_URI + " TEXT NOT NULL "
                + ");"
        );
    }

}
