package com.example.publicfeedapp.data.database.tables;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

public class PostStarsTable {

    @NonNull
    public static final String TABLE = "posts_stars";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_AUTHOR_UID = "author_uid";

    @NonNull
    public static final String COLUMN_AUTHOR_NAME = "author_name";

    @NonNull
    public static final String COLUMN_POST_ID = "post_id";


    // This is just class with Meta Data, we don't need instances
    private PostStarsTable() {
        throw new IllegalStateException("No instances please");
    }

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_POST_ID + " TEXT NOT NULL, "
                + COLUMN_AUTHOR_UID + " TEXT NOT NULL, "
                + COLUMN_AUTHOR_NAME + " TEXT NOT NULL "
                + ");"
        );
    }

}
