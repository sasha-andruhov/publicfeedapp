package com.example.publicfeedapp.data.mapper;

import com.example.publicfeedapp.model.Post;
import com.google.firebase.database.DataSnapshot;

public class PostMapper extends FirebaseMapper<Post> {

    @Override
    public Post dataItem(DataSnapshot dataSnapshot) {
        Post item = dataSnapshot.getValue(Post.class);
        item.setUid(dataSnapshot.getKey());
        return item;
    }

}
