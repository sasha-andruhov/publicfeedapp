package com.example.publicfeedapp.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.NonNull;

import com.example.publicfeedapp.data.database.tables.PostImagesTable;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.data.database.tables.PreferenceTable;

public class DbOpenHelper extends SQLiteOpenHelper {

    public DbOpenHelper(@NonNull Context context) {
        super(context, "sample_db", null, 1);
    }

    @Override
    public void onCreate(@NonNull SQLiteDatabase db) {
        PostTable.createTable(db);
        PostImagesTable.createTable(db);
        PostStarsTable.createTable(db);
        PreferenceTable.createTable(db);
    }

    @Override
    public void onUpgrade(@NonNull SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}