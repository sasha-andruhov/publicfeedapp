package com.example.publicfeedapp.data.repository;

import com.example.publicfeedapp.data.mapper.FirebaseMapper;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.List;

public abstract class FirebaseDatabaseRepository<Model> {

    protected Query databaseReference;
//    protected DatabaseReference databaseReference;
    protected FirebaseDatabaseRepositoryCallback<Model> firebaseCallback;
    private BaseValueEventListener listener;
    private FirebaseMapper mapper;

    protected abstract String getRootNode();

    public FirebaseDatabaseRepository(FirebaseMapper mapper) {
        this.mapper = mapper;
    }

    public void getData(long dateUpdate){
        databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode())
                .orderByChild("updatedAt").startAt(dateUpdate);
    }

    public void changeListenerDateUpdate(long dateUpdate){
        databaseReference = FirebaseDatabase.getInstance().getReference(getRootNode())
                .orderByChild("updatedAt").startAt(dateUpdate);
        if(listener!=null)
            databaseReference.addValueEventListener(listener);

    }

    public void addListener(FirebaseDatabaseRepositoryCallback<Model> firebaseCallback) {
        this.firebaseCallback = firebaseCallback;
        listener = new BaseValueEventListener(mapper, firebaseCallback);
        databaseReference.addValueEventListener(listener);
    }

    public void removeListener() {
        if(listener!=null)
            databaseReference.removeEventListener(listener);
    }

    public interface FirebaseDatabaseRepositoryCallback<T> {
        void onSuccess(List<T> result);
        void onError(Exception e);
    }
}
