package com.example.publicfeedapp.data.database.tables;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

public class PostTable {

    @NonNull
    public static final String TABLE = "posts";

    @NonNull
    public static final String COLUMN_ID = "_uid";

    @NonNull
    public static final String COLUMN_AUTHOR_UID = "author_uid";

    @NonNull
    public static final String COLUMN_AUTHOR_NAME = "author_name";
    @NonNull
    public static final String COLUMN_AUTHOR_PHOTO_URI = "author_photo_uri";
    @NonNull
    public static final String COLUMN_TYPE = "type";
    @NonNull
    public static final String COLUMN_BODY = "body";
    @NonNull
    public static final String COLUMN_DATE = "date";
    @NonNull
    public static final String COLUMN_LATITUDE = "latitude";
    @NonNull
    public static final String COLUMN_LONGITUDE = "longitude";
    @NonNull
    public static final String COLUMN_STAR_COUNT = "star_count";


    // This is just class with Meta Data, we don't need instances
    private PostTable() {
        throw new IllegalStateException("No instances please");
    }

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE + "("
                + COLUMN_ID + " TEXT NOT NULL PRIMARY KEY, "
                + COLUMN_AUTHOR_UID + " TEXT NOT NULL, "
                + COLUMN_AUTHOR_NAME + " TEXT NOT NULL, "
                + COLUMN_AUTHOR_PHOTO_URI + " TEXT, "
                + COLUMN_TYPE + " INTEGER NOT NULL, "
                + COLUMN_BODY + " TEXT, "
                + COLUMN_DATE + " INTEGER NOT NULL, "
                + COLUMN_LATITUDE + " REAL, "
                + COLUMN_LONGITUDE + " REAL, "
                + COLUMN_STAR_COUNT + " INTEGER "
                + ");"
        );
    }

}
