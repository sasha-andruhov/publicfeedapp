package com.example.publicfeedapp.data.database.resolvers;

import android.content.ContentValues;

import androidx.annotation.NonNull;

import com.example.publicfeedapp.data.database.entities.PostLike;
import com.example.publicfeedapp.data.database.tables.PostImagesTable;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.data.database.tables.PreferenceTable;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio3.sqlite.queries.UpdateQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreferenceRelationsPutResolver extends DefaultPutResolver<Long> {

    @Override
    @NonNull
    public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull Long dateUpdate) {
        return super.performPut(storIOSQLite, dateUpdate);
    }

    @NonNull
    @Override
    protected InsertQuery mapToInsertQuery(@NonNull Long dateUpdate) {
        return InsertQuery.builder()
                .table(PreferenceTable.TABLE)
                .build();
    }

    @NonNull
    @Override
    protected UpdateQuery mapToUpdateQuery(@NonNull Long dateUpdate) {
        return UpdateQuery.builder()
                .table(PreferenceTable.TABLE)
                .where(PreferenceTable.COLUMN_ID+" = ?")
                .whereArgs(1)
                .build();
    }

    @NonNull
    @Override
    protected ContentValues mapToContentValues(@NonNull Long dateUpdate) {
        ContentValues addContentValues = new ContentValues();
        addContentValues.put(PreferenceTable.COLUMN_ID, 1);
        addContentValues.put(PreferenceTable.COLUMN_DATE_UPDATE, dateUpdate);
        return addContentValues;
    }
}