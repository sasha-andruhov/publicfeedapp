package com.example.publicfeedapp.data.database.tables;

import android.database.sqlite.SQLiteDatabase;

import androidx.annotation.NonNull;

public class PreferenceTable {

    @NonNull
    public static final String TABLE = "preference";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_DATE_UPDATE = "date_update";

    // This is just class with Meta Data, we don't need instances
    private PreferenceTable() {
        throw new IllegalStateException("No instances please");
    }

    public static void createTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE + "("
                + COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY, "
                + COLUMN_DATE_UPDATE + " INTEGER NOT NULL DEFAULT 0 "
                + ");"
        );
    }

}
