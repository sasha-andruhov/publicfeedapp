package com.example.publicfeedapp.data.database.resolvers;

import android.database.Cursor;

import androidx.annotation.NonNull;

import com.example.publicfeedapp.data.database.entities.PostLike;
import com.example.publicfeedapp.data.database.tables.PostImagesTable;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.data.database.tables.PreferenceTable;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio3.sqlite.operations.get.GetResolver;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PreferenceRelationsGetResolver extends DefaultGetResolver<Long> {

    @Override
    @NonNull
    public Long mapFromCursor(@NonNull StorIOSQLite storIOSQLite, @NonNull Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndex(PreferenceTable.COLUMN_DATE_UPDATE));
    }
}