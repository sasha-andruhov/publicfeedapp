package com.example.publicfeedapp.data.database.resolvers;

import android.content.ContentValues;
import android.database.Cursor;

import androidx.annotation.NonNull;

import com.example.publicfeedapp.data.database.entities.PostLike;
import com.example.publicfeedapp.data.database.tables.PostImagesTable;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio3.sqlite.operations.get.GetResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.DefaultPutResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResolver;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio3.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio3.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio3.sqlite.queries.InsertQuery;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;
import com.pushtorefresh.storio3.sqlite.queries.UpdateQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;

public class PostRelationsPutResolver extends DefaultPutResolver<Post> {

    @Override
    @NonNull
    public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull final Post post) {
        final StorIOSQLite.LowLevel lowLevel = storIOSQLite.lowLevel();
        lowLevel.beginTransaction();
        try {
            final PutResult putResult = super.performPut(storIOSQLite, post);
            if(post.getType()==PostType.IMAGE){
                storIOSQLite.delete()
                        .byQuery(DeleteQuery.builder().table(PostImagesTable.TABLE)
                                .where(PostImagesTable.COLUMN_POST_ID + " = ?")
                                .whereArgs(post.getUid())
                                .build())
                        .prepare()
                        .executeAsBlocking();


                PutResolver<String> putImageResolver = new DefaultPutResolver<String>() {
                    @Override @NonNull public InsertQuery mapToInsertQuery(@NonNull String path) {
                        return InsertQuery.builder()
                                .table(PostImagesTable.TABLE)
                                .build();
                    }

                    @Override @NonNull public UpdateQuery mapToUpdateQuery(@NonNull String path) {
                        return UpdateQuery.builder()
                                .table(PostImagesTable.TABLE)
                                .where(PostImagesTable.COLUMN_IMAGE_URI+" = ?")
                                .whereArgs(path)
                                .build();
                    }

                    @Override @NonNull public ContentValues mapToContentValues(@NonNull String path) {
                        final ContentValues contentValues = new ContentValues();
                        contentValues.put(PostImagesTable.COLUMN_POST_ID,post.getUid());
                        contentValues.put(PostImagesTable.COLUMN_IMAGE_URI,path);
                        return contentValues;
                    }
                };
                storIOSQLite
                        .put()
                        .objects(post.getImages())
                        .withPutResolver(putImageResolver) // requires PutResolver<ContentValues>
                        .prepare()
                        .executeAsBlocking();
            }
            storIOSQLite.delete()
                    .byQuery(DeleteQuery.builder().table(PostStarsTable.TABLE)
                            .where(PostStarsTable.COLUMN_POST_ID + " = ?")
                            .whereArgs(post.getUid())
                            .build())
                    .prepare()
                    .executeAsBlocking();
            if(post.getStars()!=null && post.getStars().size()>0){
                List<PostLike> likes = new ArrayList<>();
                for (Map.Entry<String, String> pair: post.getStars().entrySet()) {
                    likes.add(new PostLike(null,pair.getKey(),pair.getValue(),post.getUid()));
                }
                PutResolver<PostLike> putLikesResolver = new DefaultPutResolver<PostLike>() {
                    @Override @NonNull public InsertQuery mapToInsertQuery(@NonNull PostLike like) {
                        return InsertQuery.builder()
                                .table(PostStarsTable.TABLE)
                                .build();
                    }

                    @Override @NonNull public UpdateQuery mapToUpdateQuery(@NonNull PostLike like) {
                        return UpdateQuery.builder()
                                .table(PostStarsTable.TABLE)
                                .where(PostStarsTable.COLUMN_ID+" = ?")
                                .whereArgs(like.getId())
                                .build();
                    }

                    @Override @NonNull public ContentValues mapToContentValues(@NonNull PostLike like) {
                        final ContentValues contentValues = new ContentValues();
                        contentValues.put(PostStarsTable.COLUMN_POST_ID,post.getUid());
                        contentValues.put(PostStarsTable.COLUMN_AUTHOR_UID,like.getAuthorUid());
                        contentValues.put(PostStarsTable.COLUMN_AUTHOR_NAME,like.getAuthorName());
                        return contentValues;
                    }
                };
                storIOSQLite
                        .put()
                        .objects(likes)
                        .withPutResolver(putLikesResolver) // requires PutResolver<ContentValues>
                        .prepare()
                        .executeAsBlocking();

            }
            lowLevel.setTransactionSuccessful();
            return putResult;
        } finally {
            lowLevel.endTransaction();
        }
    }

    @NonNull
    @Override
    protected InsertQuery mapToInsertQuery(@NonNull Post object) {
        return InsertQuery.builder()
                .table(PostTable.TABLE)
                .build();
    }

    @NonNull
    @Override
    protected UpdateQuery mapToUpdateQuery(@NonNull Post object) {
        return UpdateQuery.builder()
                .table(PostTable.TABLE)
                .where(PostTable.COLUMN_ID+" = ?")
                .whereArgs(object.getUid())
                .build();
    }

    @NonNull
    @Override
    protected ContentValues mapToContentValues(@NonNull Post object) {
        ContentValues addContentValues = new ContentValues();
        addContentValues.put(PostTable.COLUMN_ID, object.getUid());
        addContentValues.put(PostTable.COLUMN_AUTHOR_UID, object.getAuthorUid());
        addContentValues.put(PostTable.COLUMN_AUTHOR_NAME, object.getAuthorName());
        addContentValues.put(PostTable.COLUMN_AUTHOR_PHOTO_URI, object.getAuthorPhotoUri());
        addContentValues.put(PostTable.COLUMN_TYPE, object.getType());
        addContentValues.put(PostTable.COLUMN_BODY, object.getBody());
        addContentValues.put(PostTable.COLUMN_DATE, object.getDate());
        addContentValues.put(PostTable.COLUMN_LATITUDE, object.getLatitude());
        addContentValues.put(PostTable.COLUMN_LONGITUDE, object.getLongitude());
        addContentValues.put(PostTable.COLUMN_STAR_COUNT, object.getStarCount());
        return addContentValues;
    }
}