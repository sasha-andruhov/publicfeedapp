package com.example.publicfeedapp.data.repository;


import com.example.publicfeedapp.data.mapper.PostMapper;
import com.example.publicfeedapp.model.Post;

public class PostRepository extends FirebaseDatabaseRepository<Post> {

    public PostRepository() {
        super(new PostMapper());
    }

    @Override
    protected String getRootNode() {
        return "posts";
    }
}
