package com.example.publicfeedapp.data.database.entities

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteColumn
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteCreator
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteType
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.List
import kotlin.collections.Map
import kotlin.collections.MutableMap
import kotlin.collections.mapOf

@IgnoreExtraProperties
@StorIOSQLiteType(table = "posts_stars")
data class PostLike @StorIOSQLiteCreator constructor(
//data class PostDb (
        @StorIOSQLiteColumn(name = "_id", key = true) var id: Int?,
        @StorIOSQLiteColumn(name = "author_uid") var authorUid: String? = "",
        @StorIOSQLiteColumn(name = "author_name")  var authorName: String? = "",
        @StorIOSQLiteColumn(name = "post_id")   var postId: String? = "",
){
    override fun equals(other: Any?): Boolean {
        val post = other as PostLike
        return id==post.id;
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "authorUid" to authorUid,
                "authorName" to authorName,
                "postId" to postId
        )
    }

}

