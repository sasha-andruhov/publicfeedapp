package com.example.publicfeedapp.data.database.resolvers;

import android.database.Cursor;

import androidx.annotation.NonNull;

import com.example.publicfeedapp.data.database.entities.PostLike;
import com.example.publicfeedapp.data.database.tables.PostImagesTable;
import com.example.publicfeedapp.data.database.tables.PostStarsTable;
import com.example.publicfeedapp.data.database.tables.PostTable;
import com.example.publicfeedapp.model.Post;
import com.example.publicfeedapp.model.PostType;
import com.pushtorefresh.storio3.sqlite.StorIOSQLite;
import com.pushtorefresh.storio3.sqlite.operations.get.DefaultGetResolver;
import com.pushtorefresh.storio3.sqlite.operations.get.GetResolver;
import com.pushtorefresh.storio3.sqlite.queries.Query;
import com.pushtorefresh.storio3.sqlite.queries.RawQuery;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PostRelationsGetResolver  extends DefaultGetResolver<Post> {

    @Override
    @NonNull
    public Post mapFromCursor(@NonNull StorIOSQLite storIOSQLite, @NonNull Cursor cursor) {
        final Post post = new Post(
                cursor.getString(cursor.getColumnIndex(PostTable.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(PostTable.COLUMN_AUTHOR_UID)),
                cursor.getString(cursor.getColumnIndex(PostTable.COLUMN_AUTHOR_NAME)),
                cursor.getString(cursor.getColumnIndex(PostTable.COLUMN_AUTHOR_PHOTO_URI)),
                cursor.getInt(cursor.getColumnIndex(PostTable.COLUMN_TYPE)),
                cursor.getString(cursor.getColumnIndex(PostTable.COLUMN_BODY)),
                cursor.getLong(cursor.getColumnIndex(PostTable.COLUMN_DATE)),
                cursor.getDouble(cursor.getColumnIndex(PostTable.COLUMN_LATITUDE)),
                cursor.getDouble(cursor.getColumnIndex(PostTable.COLUMN_LONGITUDE)),
                cursor.getInt(cursor.getColumnIndex(PostTable.COLUMN_STAR_COUNT)),
                new HashMap(),
                new ArrayList<String>()
        );


        if(post.getType()== PostType.IMAGE){
            GetResolver<String> imagesResolver = new DefaultGetResolver<String>() {
                @Override
                @NonNull
                public String mapFromCursor(@NonNull StorIOSQLite storIOSQLite,@NonNull Cursor cursor) {
                    return cursor.getString(cursor.getColumnIndex(PostImagesTable.COLUMN_IMAGE_URI)); // parse Cursor here
                }
            };
            final List<String> images = storIOSQLite
                    .get()
                    .listOfObjects(String.class)
                    .withQuery(
                            RawQuery.builder()
                            .query("SELECT "
                                    + PostImagesTable.COLUMN_IMAGE_URI
                                    + " FROM " + PostImagesTable.TABLE
                                    + " WHERE " + PostImagesTable.COLUMN_POST_ID+ " = ?")
                            .args(post.getUid())
                            .build())
                    .withGetResolver(imagesResolver)
                    .prepare()
                    .executeAsBlocking();
            post.setImages(images);
        }
        GetResolver<PostLike> likesResolver = new DefaultGetResolver<PostLike>() {
            @Override
            @NonNull
            public PostLike mapFromCursor(@NonNull StorIOSQLite storIOSQLite,@NonNull Cursor cursor) {
                return new PostLike(
                        cursor.getInt(cursor.getColumnIndex(PostStarsTable.COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(PostStarsTable.COLUMN_AUTHOR_UID)),
                        cursor.getString(cursor.getColumnIndex(PostStarsTable.COLUMN_AUTHOR_NAME)),
                        cursor.getString(cursor.getColumnIndex(PostStarsTable.COLUMN_POST_ID))
                );
            }
        };
        final List<PostLike> likes = storIOSQLite
                .get()
                .listOfObjects(PostLike.class)
                .withQuery(RawQuery.builder()
                        .query("SELECT * "
                                + " FROM " + PostStarsTable.TABLE
                                + " WHERE " + PostStarsTable.COLUMN_POST_ID+ " = ?")
                        .args(post.getUid())
                        .build())
                .withGetResolver(likesResolver)
                .prepare()
                .executeAsBlocking();
        for (PostLike item : likes) {
            post.getStars().put(item.getAuthorUid(), item.getAuthorName());
        }
        return post;
    }


}