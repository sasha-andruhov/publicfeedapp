package com.example.publicfeedapp.data.mapper;

import com.google.firebase.database.DataSnapshot;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public abstract class FirebaseMapper<Model> {

    public Model dataItem(DataSnapshot dataSnapshot) {
        Model item = dataSnapshot.getValue(getModelClass());
        return item;
    }

    public List<Model> mapList(DataSnapshot dataSnapshot) {
        List<Model> list = new ArrayList<>();
        for (DataSnapshot item : dataSnapshot.getChildren()) {
            list.add(dataItem(item));
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private Class<Model> getModelClass() {
        ParameterizedType superclass = (ParameterizedType) getClass().getGenericSuperclass();
        return (Class<Model>) superclass.getActualTypeArguments()[0];
    }

}
