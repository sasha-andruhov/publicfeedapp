package com.example.publicfeedapp.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteColumn
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteCreator
import com.pushtorefresh.storio3.sqlite.annotations.StorIOSQLiteType
import java.util.*
import kotlin.collections.HashMap

@IgnoreExtraProperties
@StorIOSQLiteType(table = "posts")
data class Post @StorIOSQLiteCreator constructor(
//data class PostDb (
        @StorIOSQLiteColumn(name = "_uid", key = true) var uid: String? = "",
        @StorIOSQLiteColumn(name = "author_uid") var authorUid: String? = "",
        @StorIOSQLiteColumn(name = "author_name") var authorName: String? = "",
        @StorIOSQLiteColumn(name = "author_photo_uri") var authorPhotoUri: String? = "",
        @StorIOSQLiteColumn(name = "type") var type: Int? = PostType.TEXT,
        @StorIOSQLiteColumn(name = "body") var body: String? = "",
        @StorIOSQLiteColumn(name = "date") var date: Long = 0,
        @StorIOSQLiteColumn(name = "latitude") var latitude: Double? = 0.0,
        @StorIOSQLiteColumn(name = "longitude") var longitude: Double? = 0.0,
        @StorIOSQLiteColumn(name = "star_count") var starCount: Int? = 0,
        var stars: MutableMap<String, String>? = HashMap(),
        var images: List<String>? = ArrayList<String>(),
//            var ddd:String
){
    var updatedAt :Long? = 0;

    override fun equals(other: Any?): Boolean {
        val post = other as Post
        return uid.equals(post.uid);
    }

    @Exclude
    fun toMap(): Map<String, Any?> {
        return mapOf(
                "authorUid" to authorUid,
                "authorName" to authorName,
                "authorPhotoUri" to authorPhotoUri,
                "type" to type,
                "body" to body,
                "date" to date,
                "latitude" to latitude,
                "longitude" to longitude,
                "starCount" to starCount,
                "stars" to stars,
                "images" to images,
                "updatedAt" to (System.currentTimeMillis() / 1000).toInt()
        )
    }

}

