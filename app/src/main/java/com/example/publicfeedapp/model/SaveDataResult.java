package com.example.publicfeedapp.model;

import androidx.annotation.Nullable;


public class SaveDataResult {
    @Nullable
    private boolean success;
    @Nullable
    private String error;

    public SaveDataResult(@Nullable String error) {
        this.success = false;
        this.error = error;
    }

    public SaveDataResult(@Nullable boolean success) {
        this.success = success;
    }

    @Nullable
    public boolean getSuccess() {
        return success;
    }

    @Nullable
    public String getError() {
        return error;
    }
}