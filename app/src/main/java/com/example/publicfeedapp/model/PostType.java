package com.example.publicfeedapp.model;

public class PostType {
    public static final int TEXT = 0;
    public static final int IMAGE = 1;
    public static final int LOCATION = 2;

}
